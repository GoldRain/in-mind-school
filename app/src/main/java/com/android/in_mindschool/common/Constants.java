package com.android.in_mindschool.common;

public class Constants {

    public static final int VOLLEY_TIME_OUT = 60000;
    public static boolean g_isAppRunning = false;
    public static final String PREF_KEY_TOKEN = "PREF_KEY_TOKEN";
    public static final int PROFILE_IMAGE_SIZE = 1024;
    public static final int SPLASH_TIME = 1000;
    public static final int PAY_SUCCESS_TIME = 3000;
    public static boolean fromCreatePage = false;
    public static boolean fromGroup = false;
    public static boolean fromPost = false;
    public static boolean fromEvent = false;
    public static boolean fromPage = false;

    //PREF KEYS
    public static final String PREFKEY_USEREMAIL = "user_email";
    public static final String PREFKEY_USERPWD = "user_pwd";
    public static final String PREFKEY_TYPE = "user_type";

    public static final String KEY_LOGOUT = "logout";

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final int MY_REQUEST_CODE = 104 ;
    public static final int PICK_FROM_IMAGES = 116;
    public static final int PICK_FROM_VIDEO = 105;
    public static final int PICK_FROM_VIDEO_GALLERY = 106;
    public static final int CREATE_CLASS = 502;

    public static final String PARENT = "parent";
    public static final String SCHOOL = "school";

    public static final String KEY_IMAGES = "key_images";
    public static final String KEY_VIDEOS = "key_images";
    public static final String KEY_COUNT = "count";
    public static final String KEY_FRIEND = "key_friends";
    public static final String KEY_POSITION = "key_position";
    public static final String KEY_EMAIL = "key_email";
    public static final String KEY_VIDEOPATH = "video_path";
    public static final String KEY_NUM = "key_number";
    public static final String KEY_TYPE = "key_type";
    public static final String KEY_GROUP = "key_group";
    public static final String KEY_FROM_ACTIVITY = "key_from_activity";
    public static final String KEY_FROM_MAIN = "key_from_main";
    public static final String KEY_FILTER = "key_filter";
    public static final String FILTER_STATE = "filter_state";
    public static final String FILTER_CITY = "filter_city";
    public static final String FILTER_TAG = "filter_tag";
    public static final int FILTER_CODE = 502;


}
