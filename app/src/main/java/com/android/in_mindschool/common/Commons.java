package com.android.in_mindschool.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.TypedValue;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Commons {


    public static String fileNameWithExtFromUrl(String url) {

        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }

        if (url.lastIndexOf("/") == -1) {
            return url;
        } else {
            String name = url.substring(url.lastIndexOf("/")  + 1);
            return name;
        }
    }

    public static String fileNameWithoutExtFromUrl(String url) {

        String fullname = fileNameWithExtFromUrl(url);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }

    public static String fileNameWithExtFromPath(String path) {

        if (path.lastIndexOf("/") > -1)
            return path.substring(path.lastIndexOf("/") + 1);

        return path;
    }

    public static int GetPixelValueFromDp(Context context, float dp_value) {

        int pxValue = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp_value, context.getResources()
                        .getDisplayMetrics());

        return pxValue;
    }
    public static String getDurationString(int duration) {

        int seconds = duration / 1000;
        int mins = seconds / 60;
        int hours = mins / 60;

        if (hours > 0) {
            return String.format("%d:%02d:%02d", hours, mins % 60, seconds % 60);
        } else {
            return String.format("%02d:%02d", mins % 60, seconds % 60);
        }
    }

    public static String getThumbnail(String _videoPath){

        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(_videoPath, MediaStore.Images.Thumbnails.MINI_KIND);
        return getThumbPath(thumb);
    }

    private static String getThumbPath(Bitmap thumb) {

        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Temp";
        File dir = new File(file_path);
        if(!dir.exists())
            dir.mkdirs();

        File file = new File(dir,  "temp_ex" + System.currentTimeMillis()  + ".png");

        try {

            FileOutputStream fOut = new FileOutputStream(file);

            thumb.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return file.getAbsolutePath();
    }

    public static String fileNameWithoutExtFromPath(String path) {

        String fullname = fileNameWithExtFromPath(path);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }

}
