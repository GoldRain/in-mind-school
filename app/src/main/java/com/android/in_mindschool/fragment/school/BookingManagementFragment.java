package com.android.in_mindschool.fragment.school;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.school.SchoolMainActivity;
import com.android.in_mindschool.adapter.EnrolledStudentsListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookingManagementFragment extends Fragment {

    SchoolMainActivity activity;
    ImageView imv_class;
    TextView txv_class_name, txv_class_des, txv_type, txv_students, txv_attendance;

    @BindView(R.id.lst_booking_manage) ListView lst_booking_manage;
    EnrolledStudentsListAdapter adapter;

    public BookingManagementFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking_management, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        View headerView = ((LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                inflate(R.layout.header_booking_management, null, false);

        imv_class = (ImageView)headerView.findViewById(R.id.imv_class);
        txv_class_name = (TextView)headerView.findViewById(R.id.txv_class_name);
        txv_class_des = (TextView)headerView.findViewById(R.id.txv_class_des);
        //imv_type = (ImageView) headerView.findViewById(R.id.imv_type);
        txv_type = (TextView)headerView.findViewById(R.id.txv_type);
        txv_students = (TextView)headerView.findViewById(R.id.txv_students);
        txv_attendance = (TextView)headerView.findViewById(R.id.txv_attendance);

        lst_booking_manage.addHeaderView(headerView);
        adapter = new EnrolledStudentsListAdapter(getContext());
        lst_booking_manage.setAdapter(adapter);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (SchoolMainActivity) context;
    }
}