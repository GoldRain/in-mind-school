package com.android.in_mindschool.fragment.parent;

import android.content.Context;
import android.media.Image;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.parent.ParentMainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment implements View.OnClickListener{

    ParentMainActivity activity;

    @BindView(R.id.imv_user) CircleImageView imv_user;
    @BindView(R.id.edt_name) EditText edt_name;
    @BindView(R.id.edt_location) EditText edt_location;
    @BindView(R.id.edt_phone) EditText edt_phone;
    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_about_us) EditText edt_about_us;
    @BindView(R.id.btn_take_membership) Button btn_btn_take_membership;

    ImageView imv_edit;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout(){

        imv_edit = (ImageView) activity.findViewById(R.id.imv_edit);
        imv_edit.setOnClickListener(this);

        edt_name.setEnabled(false);
        edt_location.setEnabled(false);
        edt_phone.setEnabled(false);
        edt_email.setEnabled(false);
        edt_about_us.setEnabled(false);

        btn_btn_take_membership.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (ParentMainActivity)context;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_edit:

                edt_name.setEnabled(true);
                edt_location.setEnabled(true);
                edt_phone.setEnabled(true);
                edt_email.setEnabled(true);
                edt_about_us.setEnabled(true);

                activity.showToast("Edit");
                break;

            case R.id.btn_take_membership:
                activity.showToast("TakeMembership");
                break;
        }
    }
}
