package com.android.in_mindschool.fragment.school;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.school.SchoolMainActivity;
import com.android.in_mindschool.adapter.NotificationListAdapter;
import com.android.in_mindschool.model.NotificationModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationFragment extends Fragment {

    SchoolMainActivity activity;
    @BindView(R.id.lst_notification)
    ListView lst_notification;
    NotificationListAdapter adapter;
    ArrayList<NotificationModel> allNotification = new ArrayList<>();

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_notification2, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        adapter = new NotificationListAdapter(getContext(), allNotification);
        lst_notification.setAdapter(adapter);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (SchoolMainActivity) context;
    }
}