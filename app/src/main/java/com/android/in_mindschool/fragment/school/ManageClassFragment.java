package com.android.in_mindschool.fragment.school;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.school.CreateClassActivity;
import com.android.in_mindschool.activities.school.SchoolMainActivity;
import com.android.in_mindschool.adapter.ManageClassListAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ManageClassFragment extends Fragment {

    SchoolMainActivity activity;
    @BindView(R.id.lst_manage_class) ListView lst_manage_class;
    @BindView(R.id.fbtn_add_class) FloatingActionButton fbtn_add_class;

    ManageClassListAdapter adapter;

    public ManageClassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_class, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        fbtn_add_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, CreateClassActivity.class);
                startActivity(intent);
                activity.finish();
            }
        });

        View header = ((LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                inflate(R.layout.header_manage_class, null, false);
        lst_manage_class.addHeaderView(header);
        adapter = new ManageClassListAdapter(getContext(), activity);
        lst_manage_class.setAdapter(adapter);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (SchoolMainActivity) context;
    }
}