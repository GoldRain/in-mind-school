package com.android.in_mindschool.fragment.parent;

import android.os.Bundle;

import androidx.annotation.BinderThread;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.adapter.MyClassListAdapter;
import com.android.in_mindschool.model.ClassModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyClassFragment extends Fragment {

    @BindView(R.id.btn_upcoming_classes) Button btn_upcoming_classes;
    @BindView(R.id.btn_past_classes) Button btn_past_classes;
    @BindView(R.id.lst_my_class) ListView lst_my_class;

    MyClassListAdapter adapter;
    ArrayList<ClassModel> upcomingClasses = new ArrayList<>();
    ArrayList<ClassModel> pastClasses = new ArrayList<>();

    public MyClassFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_class, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout(){

        adapter = new MyClassListAdapter(getContext());
        lst_my_class.setAdapter(adapter);
        selectStatus(true, false);
    }

    private void getAllUpComingClasses(){}
    private void getAllPastClasses(){}

    @OnClick(R.id.btn_upcoming_classes) void selectUpcomingClasses(){

        selectStatus(true, false);
        lst_my_class.setAdapter(adapter);
        adapter.setList(upcomingClasses, "Upcoming");
    }

    @OnClick(R.id.btn_past_classes) void selectPastClasses(){
        selectStatus(false, true);
        lst_my_class.setAdapter(adapter);
        adapter.setList(pastClasses, "Completed");
    }

    private void selectStatus(boolean a, boolean b){

        btn_upcoming_classes.setSelected(a);
        btn_past_classes.setSelected(b);
    }
}
