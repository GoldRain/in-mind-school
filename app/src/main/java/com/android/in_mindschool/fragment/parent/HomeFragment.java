package com.android.in_mindschool.fragment.parent;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.parent.ParentMainActivity;
import com.android.in_mindschool.adapter.HomeClassListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends Fragment {

    @BindView(R.id.lst_class) ListView lst_class;
    HomeClassListAdapter adapter;

    ParentMainActivity activity;
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout(){

        adapter = new HomeClassListAdapter(getContext(), activity);
        lst_class.setAdapter(adapter);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (ParentMainActivity)context;
    }
}
