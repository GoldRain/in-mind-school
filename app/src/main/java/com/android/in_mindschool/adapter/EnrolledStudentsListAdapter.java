package com.android.in_mindschool.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.model.UserModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class EnrolledStudentsListAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    ArrayList<UserModel> enrolledUsers = new ArrayList<>();

    public EnrolledStudentsListAdapter(Context context){
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    private void setList(ArrayList<UserModel> enrolledUsers){
        this.enrolledUsers = enrolledUsers;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();

            convertView = inflater.inflate(R.layout.item_enrolled_list, parent, false);
            viewHolder.imv_user = (CircleImageView)convertView.findViewById(R.id.imv_user);
            viewHolder.txv_name = (TextView) convertView.findViewById(R.id.txv_name);
            viewHolder.txv_phone = (TextView) convertView.findViewById(R.id.txv_phone);
            viewHolder.txv_enrolled_date = (TextView) convertView.findViewById(R.id.txv_enrolled_date);

            convertView.setTag(viewHolder);
        }
        else {

            viewHolder = (ViewHolder) convertView.getTag();
        }

       /* UserModel enrolled = (UserModel) enrolledUsers.get(position);

        Glide.with(context).load(enrolled.photoUrl).into(viewHolder.imv_user);
        viewHolder.txv_name.setText(enrolled.name);
        viewHolder.txv_phone.setText(enrolled.phoneNumber);
        viewHolder.txv_enrolled_date.setText(enrolled.enrolledDate);*/

        return convertView;
    }

    private class ViewHolder{

        CircleImageView imv_user;
        TextView txv_name, txv_phone, txv_enrolled_date;
    }
}
