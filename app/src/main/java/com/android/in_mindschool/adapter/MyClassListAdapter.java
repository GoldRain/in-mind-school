package com.android.in_mindschool.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.parent.PastClassDetailsActivity;
import com.android.in_mindschool.activities.parent.UpComingClassDetailsActivity;
import com.android.in_mindschool.model.ClassModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class MyClassListAdapter extends BaseAdapter {

    Context context;
    ArrayList<ClassModel> allClass = new ArrayList<>();
    LayoutInflater inflater;
    String status = "";

    public MyClassListAdapter(Context context){
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    public void setList(ArrayList<ClassModel> classModels, String status){
        allClass = classModels;
        this.status = status;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return 12;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){

            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_my_class_list, parent, false);

            viewHolder.imv_class = (ImageView)convertView.findViewById(R.id.imv_class);
            viewHolder.txv_class_name = (TextView)convertView.findViewById(R.id.txv_class_name);
            viewHolder.txv_class_des = (TextView)convertView.findViewById(R.id.txv_class_des);
            viewHolder.imv_type = (ImageView) convertView.findViewById(R.id.imv_type);
            viewHolder.txv_type = (TextView)convertView.findViewById(R.id.txv_type);
            viewHolder.txv_time = (TextView)convertView.findViewById(R.id.txv_time);
            viewHolder.ratingBar = (RatingBar)convertView.findViewById(R.id.rating);
            viewHolder.txv_rating = (TextView)convertView.findViewById(R.id.txv_rating);
            viewHolder.lyt_time = (LinearLayout)convertView.findViewById(R.id.lyt_time);
            viewHolder.lyt_completed = (LinearLayout)convertView.findViewById(R.id.lyt_completed);

            convertView.setTag(viewHolder);
        }
        else {

            viewHolder = (ViewHolder)convertView.getTag();
        }


        /*ClassModel classModel = (ClassModel) allClass.get(position);

        Glide.with(context).load(classModel.photoUrl).into(viewHolder.imv_class);
        viewHolder.txv_class_name.setText(classModel.name);
        viewHolder.txv_class_des.setText(classModel.description);
        viewHolder.txv_type.setText(classModel.type);
        viewHolder.txv_time.setText(classModel.time);
        viewHolder.ratingBar.setRating(classModel.rating);*/

        if (status.equalsIgnoreCase("Completed")){

            viewHolder.lyt_completed.setVisibility(View.VISIBLE);
            viewHolder.lyt_time.setVisibility(View.GONE);
        }
        else {
            viewHolder.lyt_completed.setVisibility(View.GONE);
            viewHolder.lyt_time.setVisibility(View.VISIBLE);
        }

        viewHolder.ratingBar.setRating(position%5);
        viewHolder.txv_rating.setText(viewHolder.ratingBar.getRating() + "/5");

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (status.equalsIgnoreCase("Completed")){

                    Intent intent = new Intent(context, PastClassDetailsActivity.class);
                    context.startActivity(intent);
                }
                else {

                    Intent intent = new Intent(context, UpComingClassDetailsActivity.class);
                    context.startActivity(intent);
                }
            }
        });
        return convertView;
    }

    private class ViewHolder{

        ImageView imv_class, imv_type;
        LinearLayout lyt_time, lyt_completed;
        TextView txv_class_name, txv_class_des, txv_type, txv_time, txv_rating;
        RatingBar ratingBar;
    }
}
