package com.android.in_mindschool.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.parent.ClassDetailsActivity;
import com.android.in_mindschool.activities.parent.ParentMainActivity;
import com.android.in_mindschool.model.ClassModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class HomeClassListAdapter extends BaseAdapter {

    Context context;
    ParentMainActivity activity;

    ArrayList<ClassModel> allClass = new ArrayList<>();
    LayoutInflater inflater;

    public HomeClassListAdapter(Context context, ParentMainActivity activity){
        this.context = context;
        this.activity = activity;
        this.inflater = LayoutInflater.from(context);
    }

    public void setList(ArrayList<ClassModel> classModels){

        this.allClass = classModels;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return 12;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView ==  null){

            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_class_list, parent, false);

            viewHolder.imv_class = (ImageView)convertView.findViewById(R.id.imv_class);
            viewHolder.txv_class_name = (TextView)convertView.findViewById(R.id.txv_class_name);
            viewHolder.txv_class_des = (TextView)convertView.findViewById(R.id.txv_class_des);
            viewHolder.imv_type = (ImageView) convertView.findViewById(R.id.imv_type);
            viewHolder.txv_type = (TextView)convertView.findViewById(R.id.txv_type);
            viewHolder.txv_time = (TextView)convertView.findViewById(R.id.txv_time);
            viewHolder.ratingBar = (RatingBar)convertView.findViewById(R.id.rating);
            viewHolder.txv_rating = (TextView)convertView.findViewById(R.id.txv_rating);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        /*ClassModel classModel = (ClassModel) allClass.get(position);

        Glide.with(context).load(classModel.photoUrl).into(viewHolder.imv_class);
        viewHolder.txv_class_name.setText(classModel.name);
        viewHolder.txv_class_des.setText(classModel.description);
        viewHolder.txv_type.setText(classModel.type);
        viewHolder.txv_time.setText(classModel.time);
        viewHolder.ratingBar.setRating(classModel.rating);*/

        viewHolder.ratingBar.setRating(position%5);
        viewHolder.txv_rating.setText(viewHolder.ratingBar.getRating() + "/5");

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //context.startActivity(new Intent(context, ClassDetailsActivity.class));

                Intent intent = new Intent(activity, ClassDetailsActivity.class);
                /*intent.putExtra()*/
                activity.startActivity(intent);
                activity.finish();
            }
        });

        return convertView;
    }

    private class ViewHolder{

        ImageView imv_class, imv_type;
        TextView txv_class_name, txv_class_des, txv_type, txv_time, txv_rating;
        RatingBar ratingBar;
    }
}
