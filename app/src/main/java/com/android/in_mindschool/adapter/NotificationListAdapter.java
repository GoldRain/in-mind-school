package com.android.in_mindschool.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.model.NotificationModel;

import java.util.ArrayList;

public class NotificationListAdapter extends BaseAdapter {

    Context _context;
    ArrayList<NotificationModel> allNoti = new ArrayList<>();
    LayoutInflater inflater;

    public NotificationListAdapter(Context context, ArrayList<NotificationModel> notificationModels){

        this._context = context;
        this.allNoti = notificationModels;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return 8/*allNoti.size()*/;
    }

    @Override
    public Object getItem(int position) {
        return allNoti.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        NotificationHolder holder;
        if (convertView == null) {

            holder = new NotificationHolder();

            convertView = inflater.inflate(R.layout.item_notification_list, parent, false);

            holder.txv_noti_title = (TextView) convertView.findViewById(R.id.txv_noti_title);
            holder.txv_noti_content = (TextView) convertView.findViewById(R.id.txv_noti_content);
            holder.txv_noti_date = (TextView) convertView.findViewById(R.id.txv_date);

            convertView.setTag(holder);
        }
        else {

            holder = (NotificationHolder) convertView.getTag();
        }

        /*NotificationModel noti = (NotificationModel) allNoti.get(position);

        holder.txv_noti_title.setText(noti.noti_title);
        holder.txv_noti_content.setText(noti.noti_content);
        holder.txv_noti_date.setText(noti.noti_date);*/

        return convertView;
    }

    public class NotificationHolder{

        TextView txv_noti_title, txv_noti_content, txv_noti_date;
    }
}
