package com.android.in_mindschool.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.model.RatingModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReviewListAdapter extends BaseAdapter {

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<RatingModel> ratings = new ArrayList<>();

    public ReviewListAdapter(Context context){
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null){

            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_review_list, parent,false);

            viewHolder.imv_user = (CircleImageView) convertView.findViewById(R.id.imv_user);
            viewHolder.txv_name = (TextView)convertView.findViewById(R.id.txv_name);
            viewHolder.txv_content = (TextView)convertView.findViewById(R.id.txv_content);
            viewHolder.txv_rating = (TextView) convertView.findViewById(R.id.txv_rating);
            viewHolder.txv_date = (TextView)convertView.findViewById(R.id.txv_date);
            viewHolder.rating = (RatingBar)convertView.findViewById(R.id.rating);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

       /* RatingModel rate = (RatingModel) ratings.get(position);

        Glide.with(context).load(rate.photoUrl).into(viewHolder.imv_user);
        viewHolder.txv_name.setText(rate.name);
        viewHolder.txv_content.setText(rate.content);
        viewHolder.txv_date.setText(rate.date);
        viewHolder.rating.setRating(rate.rating);
        viewHolder.txv_rating.setText(rate.rating);*/

        return convertView;
    }

    public class ViewHolder{

        CircleImageView imv_user;
        TextView txv_name, txv_content, txv_rating, txv_date;
        RatingBar rating;
    }
}
