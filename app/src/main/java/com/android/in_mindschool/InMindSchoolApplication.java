package com.android.in_mindschool;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import androidx.multidex.MultiDexApplication;

import com.pixplicity.easyprefs.library.Prefs;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.kakao.util.helper.Utility.getPackageInfo;

public class InMindSchoolApplication extends MultiDexApplication {

    public static final String TAG = InMindSchoolApplication.class.getSimpleName();
    private static InMindSchoolApplication _instance;

    @Override
    public void onCreate() {
        super.onCreate();

        //Commons.g_user = new UserModel();
        _instance = this;
        /*Firebase.setAndroidContext(this);
        FirebaseApp.initializeApp(this);*/
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

    public static void printKeyHash(final Context context) {
        PackageInfo packageInfo = getPackageInfo(context, PackageManager.GET_SIGNATURES);
        if (packageInfo == null)
            return;

        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                String hashKey = Base64.encodeToString(md.digest(), Base64.NO_WRAP);
                Log.i("KeyHash", "key:" + hashKey);
                return;
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

        }
    }

    public static synchronized InMindSchoolApplication getInstance(){

        return _instance;
    }
}
