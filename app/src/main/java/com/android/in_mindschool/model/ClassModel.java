package com.android.in_mindschool.model;

public class ClassModel {

    public int id;
    public String name;
    public String description;
    public String type;
    public String time;
    public String photoUrl;
    public String rating;
    public String status;

    public ClassModel(){}

    public ClassModel(int id, String name, String description, String type,String time,String photoUrl,String rating, String status){
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.time = time;
        this.photoUrl = photoUrl;
        this.rating = rating;
        this.status = status;
    }
}
