package com.android.in_mindschool.model;

public class UserModel {
    public int id;
    public String name;
    public String phoneNumber;
    public String photoUrl;
    public String enrolledDate;

    public UserModel(){}
    public UserModel(int id, String name, String phoneNumber, String photoUrl, String enrolledDate){

        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.photoUrl = photoUrl;
        this.enrolledDate = enrolledDate;
    }
}
