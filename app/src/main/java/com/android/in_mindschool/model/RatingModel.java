package com.android.in_mindschool.model;

public class RatingModel {

    public int id;
    public String name;
    public String content;
    public String rating;
    public String date;
    public String photoUrl;

    public RatingModel(){}

    public RatingModel(int id, String name, String content, String rating, String date, String photoUrl){
        this.id = id;
        this.name = name;
        this.content = content;
        this.rating = rating;
        this.date = date;
        this.photoUrl = photoUrl;
    }
}
