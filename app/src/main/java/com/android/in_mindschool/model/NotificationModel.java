package com.android.in_mindschool.model;

public class NotificationModel {

    public int id = 0;
    public int user_id = 0;
    public String noti_title = "";
    public String noti_content = "";
    public String noti_date = "";

    public NotificationModel(){}

    public NotificationModel(int id, int user_id, String noti_title, String noti_content, String noti_date){

        this.id = id;
        this.user_id = user_id;
        this.noti_title = noti_title;
        this.noti_content = noti_content;
        this.noti_date = noti_date;
    }
}
