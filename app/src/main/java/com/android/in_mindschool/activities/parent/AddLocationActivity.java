package com.android.in_mindschool.activities.parent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.BaseActivity;
import com.android.in_mindschool.activities.SignUpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddLocationActivity extends BaseActivity {

    @BindView(R.id.edt_location) EditText edt_location;
    @BindView(R.id.btn_check) ImageButton btn_check;
    String location = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);
        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout(){

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_location.getWindowToken(), 0);
                return false;
            }
        });
    }

    @OnClick(R.id.btn_check) void addLocation(){
        location = edt_location.getText().toString();
        if (checkValid()){
            startActivity(new Intent(this, ParentMainActivity.class));
            finish();
        }
    }

    private boolean checkValid(){
        if (location.isEmpty()){
            showToast(R.string.add_your_location);
            return false;
        }

        return true;
    }

    @OnClick(R.id.imv_back) void gotoBack(){
        startActivity(new Intent(AddLocationActivity.this, SignUpActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}
