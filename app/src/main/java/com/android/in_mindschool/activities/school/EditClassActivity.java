package com.android.in_mindschool.activities.school;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditClassActivity extends BaseActivity {

    @BindView(R.id.txv_category) TextView txv_category;
    @BindView(R.id.edt_class_name) EditText edt_class_name;
    @BindView(R.id.edt_about) EditText edt_about;
    @BindView(R.id.edt_strength) EditText edt_strength;
    @BindView(R.id.edt_fees) EditText edt_fees;
    @BindView(R.id.txv_location) TextView txv_location;
    @BindView(R.id.imv_location) ImageView imv_location;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_class);
        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout() {

    }

    @OnClick(R.id.imv_back) void gotoBack(){
        startActivity(new Intent(this, SchoolMainActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}