package com.android.in_mindschool.activities.parent;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.BaseActivity;
import com.android.in_mindschool.common.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TotalPaymentActivity extends BaseActivity {

    @BindView(R.id.txv_total_payment) TextView txv_total_payment;
    @BindView(R.id.txv_date) TextView txv_date;
    @BindView(R.id.txv_time_payment) TextView txv_time_payment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_payment);
        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout(){

    }

    @OnClick(R.id.btn_add_cart) void addToCart(){

    }

    @OnClick(R.id.btn_pay_now) void payNow(){

        showCompletedPayment();
    }

    private void showCompletedPayment(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_payment_success);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        TextView txv_payment_amount = (TextView)dialog.findViewById(R.id.txv_payment_amount);
        TextView date_payment = (TextView)dialog.findViewById(R.id.date_payment);
        TextView txv_time_payment = (TextView)dialog.findViewById(R.id.txv_time_payment);
        TextView txv_payment_id = (TextView)dialog.findViewById(R.id.txv_payment_id);

        TextView txv_cancel = (TextView)dialog.findViewById(R.id.txv_cancel);
        txv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ImageView imv_check = (ImageView) dialog.findViewById(R.id.imv_check);
        imv_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        txv_payment_amount.setText("$" + "25");

        dialog.show();
    }

    @OnClick(R.id.imv_back) void gotoBack(){
        Intent intent = new Intent(this, BookYourClassActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}