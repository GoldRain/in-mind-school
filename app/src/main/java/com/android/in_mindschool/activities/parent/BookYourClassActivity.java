package com.android.in_mindschool.activities.parent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.BaseActivity;
import com.android.in_mindschool.rangeDatePicker.CalendarPickerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookYourClassActivity extends BaseActivity{

    CalendarPickerView calendar;
    @BindView(R.id.imv_class) ImageView imv_class;
    @BindView(R.id.txv_class_name) TextView txv_class_name;
    @BindView(R.id.txv_class_des) TextView txv_class_des;
    @BindView(R.id.txv_type) TextView txv_type;
    @BindView(R.id.imv_type) ImageView imv_type;
    @BindView(R.id.txv_time) TextView txv_time;
    @BindView(R.id.rating) RatingBar rating;
    @BindView(R.id.txv_rating) TextView txv_rating;
    @BindView(R.id.lyt_class_dates) LinearLayout lyt_class_dates;
    @BindView(R.id.txv_dates) TextView txv_dates;
    @BindView(R.id.lyt_total_price) LinearLayout lyt_total_price;
    @BindView(R.id.txv_total_payment) TextView txv_total_payment;

    //@BindView(R.id.slyCalendarView) SlyCalendarView slyCalendarView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_your_class);

        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout() {

        initCalendarView(); // Calendar view initialization
    }

    private void initCalendarView(){

        calendar = findViewById(R.id.calendar_view);
        /*Last year*/
        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);
        /*Next year*/
        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, 0);

        ArrayList<Integer> list = new ArrayList<>();

        ArrayList<Date> arrayList = new ArrayList<>();
        try {
            SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
            String strdate = "22-4-2019";
            Date newdate = dateformat.parse(strdate);
            arrayList.add(newdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.scrollToDate(new Date());
        calendar.init(lastYear.getTime(), nextYear.getTime(), new SimpleDateFormat("MMM, yyyy", Locale.getDefault())/*, TimeZone.getTimeZone(Commons.thisSeller.getTimezone())*/) //
                .inMode(CalendarPickerView.SelectionMode.RANGE) //
                .withDeactivateDates(list);
        //.withSubTitles(getSubTitles())
        //.withHighlightedDates(list);

        calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                calendar.getSelectedDates();
                Log.d("dates", calendar.getSelectedDates().toString());

                if (calendar.getSelectedDates().size() == 1){
                    String startDate = calendar.getSelectedDates().get(0).toString().substring(0, 11);
                    txv_dates.setText(startDate);
                }
                else {
                    int idx = calendar.getSelectedDates().size();
                    String dateArray[] = calendar.getSelectedDates().get(0).toString().split(" ");

                    String startDate = calendar.getSelectedDates().get(0).toString().substring(0, 11);
                    String endDate = calendar.getSelectedDates().get(idx - 1).toString().substring(0, 11);
                    txv_dates.setText(startDate +" - " + endDate);

                }
            }

            @Override
            public void onDateUnselected(Date date) {

            }
        });

    }

    @OnClick(R.id.btn_pay_book) void payBook(){

        Intent intent = new Intent(this, TotalPaymentActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.imv_back) void gotoBack(){
        Intent intent = new Intent(this, ClassDetailsActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {

        gotoBack();
    }
}