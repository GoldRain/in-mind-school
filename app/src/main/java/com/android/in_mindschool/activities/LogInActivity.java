package com.android.in_mindschool.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.parent.AddLocationActivity;
import com.android.in_mindschool.activities.parent.ParentMainActivity;
import com.android.in_mindschool.activities.school.CreateProfileActivity;
import com.android.in_mindschool.activities.school.SchoolMainActivity;
import com.android.in_mindschool.common.Constants;
import com.pixplicity.easyprefs.library.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LogInActivity extends BaseActivity {

    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_password) EditText edt_password;
    @BindView(R.id.txv_parent) TextView txv_parent;
    @BindView(R.id.txv_school) TextView txv_school;
    String email = "", password = "";
    boolean _isFromLogout = false;

    String _type = Constants.PARENT;

    //private CallbackManager callbackManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        ButterKnife.bind(this);
        initValue();
        loadLayout();
    }

    private void initValue(){

        _isFromLogout = getIntent().getBooleanExtra(Constants.KEY_LOGOUT, false);
    }

    private void loadLayout() {

        //printKeyHash(this);
        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
                return false;
            }
        });

        if (_isFromLogout){

            Prefs.putString(Constants.PREFKEY_USEREMAIL, "");
            Prefs.putString(Constants.PREFKEY_USERPWD, "");
            Prefs.putString(Constants.PREFKEY_TYPE, "");

            edt_email.setText("");
            edt_password.setText("");
        }
        else {

            String _email = Prefs.getString( Constants.PREFKEY_USEREMAIL, "");
            String _password = Prefs.getString(Constants.PREFKEY_USERPWD, "");
            _type = Prefs.getString(Constants.PREFKEY_TYPE, "");

            edt_email.setText(_email);
            edt_password.setText(_password);

            if (Constants.PARENT.equals(_type))
                txv_parent.setSelected(true);
            else if (Constants.SCHOOL.equals(_type))
                txv_school.setSelected(true);
            else txv_parent.setSelected(true);

            if ( _email.length()>0 && _password.length() > 0 ) {

                email = edt_email.getText().toString();
                password = edt_password.getText().toString();

                //processLogin(_type);
            }
        }

        //txv_buyer.setSelected(true);
    }

    @OnClick(R.id.txv_parent) void selectParent(){

        /*txv_school.setSelected(false);
        txv_parent.setSelected(true);
        txv_parent.setTextColor(getResources().getColor(R.color.white));
        txv_school.setTextColor(getResources().getColor(R.color.colorPrimary));*/

        selectButton(false, true, getResources().getColor(R.color.white), getResources().getColor(R.color.colorPrimary));
    }

    @OnClick(R.id.txv_school) void selectSchool(){

        selectButton(true, false, getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.white));
    }

    private boolean checkValid(){

        if (_type.isEmpty()){

            showToast("Select type");
            return false;

        }else if (email.isEmpty()){

            showToast(getString(R.string.input_your_email));
            return false;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            showToast(R.string.valid_email);
            return false;
        }
        else if (password.isEmpty()){

            showToast(getString(R.string.input_password));
            return false;
        }

        return true;
    }

    @OnClick(R.id.btn_login) void gotoLogin(){

        email = edt_email.getText().toString();
        password = edt_password.getText().toString();

        getType();

        /*if (checkValid()){*/

            if (txv_parent.isSelected()){

                //processLogin(Constants.BUYER);
                startActivity(new Intent(LogInActivity.this, ParentMainActivity.class));
                finish();
            }
            else if (txv_school.isSelected()){

                //showToast("Coming soon");
                //processLogin(Constants.COURIER);
                startActivity(new Intent(LogInActivity.this, SchoolMainActivity.class));
                finish();
            //}
        }
    }

    @OnClick(R.id.txv_signUp) void gotoSignUp(){

        startActivity(new Intent(LogInActivity.this, SignUpActivity.class));
        finish();
    }

    @OnClick(R.id.txv_forgot_pass) void gotoForgotPassword(){

        startActivity(new Intent(LogInActivity.this, ForgotPasswordActivity.class));
        finish();

    }


    private void getType(){

        if (txv_parent.isSelected())
            _type = Constants.PARENT;
        else if (txv_school.isSelected())
            _type = Constants.SCHOOL;

    }

    private void selectButton(boolean a, boolean b, int c, int d){

        txv_school.setSelected(a);
        txv_parent.setSelected(b);
        txv_parent.setTextColor(c);
        txv_school.setTextColor(d);

    }

    @Override
    public void onBackPressed() {
        onExit();
    }
}
