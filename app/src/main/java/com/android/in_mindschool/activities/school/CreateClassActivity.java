package com.android.in_mindschool.activities.school;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.BaseActivity;
import com.android.in_mindschool.common.Constants;
import com.android.in_mindschool.utils.BitmapUtils;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateClassActivity extends BaseActivity {

    @BindView(R.id.imv_class) ImageView imv_class;
    @BindView(R.id.imv_upload) ImageView imv_upload;
    @BindView(R.id.txv_upload) TextView txv_upload;

    @BindView(R.id.txv_category) TextView txv_category;
    @BindView(R.id.edt_class_name) EditText edt_class_name;
    @BindView(R.id.edt_about) EditText edt_about;
    @BindView(R.id.edt_strength) EditText edt_strength;
    @BindView(R.id.edt_fees) EditText edt_fees;
    @BindView(R.id.txv_save_continue) TextView txv_save_continue;

    Uri _imageCaptureUri;
    String _photoPath = "";
    String category = "", class_name = "", about_class = "", class_fees = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_class);
        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout() {

    }

    @OnClick(R.id.imv_class) void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {
                    doTakePhoto();

                } else if (item == 1){
                    doTakeGallery();

                } else return;
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        _photoPath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(_photoPath));
        _imageCaptureUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(_photoPath));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case Crop.REQUEST_CROP: {

                if (resultCode == RESULT_OK){

                    try {

                        File outFile = BitmapUtils.getOutputMediaFile(this, "temp.png");

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(outFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        ExifInterface ei = new ExifInterface(outFile.getAbsolutePath());
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                        Bitmap returnedBitmap = bitmap;

                        switch (orientation) {

                            case ExifInterface.ORIENTATION_ROTATE_90:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 90);
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 180);
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 270);
                                bitmap.recycle();
                                bitmap = null;
                                break;

                            default:
                                break;
                        }

                        Bitmap w_bmpSizeLimited = Bitmap.createScaledBitmap(returnedBitmap, returnedBitmap.getWidth(), returnedBitmap.getHeight(), true);
                        File newFile = BitmapUtils.getOutputMediaFile(this, System.currentTimeMillis() + ".png");
                        BitmapUtils.saveOutput(newFile, w_bmpSizeLimited);
                        _photoPath = newFile.getAbsolutePath();

                        Log.d("photopath===", _photoPath);
                        // imv_avatar(_photoPath);

                        if (_photoPath.isEmpty()){
                            imv_upload.setVisibility(View.VISIBLE);
                            txv_upload.setVisibility(View.VISIBLE);
                        }
                        else {
                            imv_upload.setVisibility(View.GONE);
                            txv_upload.setVisibility(View.GONE);
                        }

                        imv_class.setImageBitmap(w_bmpSizeLimited);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
            }

            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();

                    beginCrop(_imageCaptureUri);
                }
                break;

            case Constants.PICK_FROM_CAMERA:
            {
                if (resultCode == RESULT_OK){
                    try {
                        String filename = "IMAGE_" + System.currentTimeMillis() + ".jpg";

                        Bitmap bitmap = BitmapUtils.loadOrientationAdjustedBitmap(_photoPath);
                        String w_strFilePath = "";
                        String w_strLimitedImageFilePath = BitmapUtils.getUploadImageFilePath(bitmap, filename);
                        if (w_strLimitedImageFilePath != null) {
                            w_strFilePath = w_strLimitedImageFilePath;
                        }

                        _photoPath = w_strFilePath;
                        _imageCaptureUri = Uri.fromFile(new File(_photoPath));
                        //  _photoPath= BitmapUtils.getSizeLimitedImageFilePath(_photoPath);
                        _imageCaptureUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(_photoPath));

                        beginCrop(_imageCaptureUri);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                }
                break;
            }

            case Constants.CREATE_CLASS:

                if (resultCode == RESULT_OK) {
                    startActivity(new Intent(this, SchoolMainActivity.class));
                    finish();
                }

                break;

            default: break;

        }
    }

    private void beginCrop(Uri source) {

        Uri destination = Uri.fromFile(BitmapUtils.getOutputMediaFile(this, "temp.png"));
        Crop.of(source, destination).withMaxSize(840, 1024).start(this);
    }

    @OnClick(R.id.txv_save_continue) void gotoNextStep(){

        Intent intent = new Intent(this, CreateClass2Activity.class);
        startActivityForResult(intent, Constants.CREATE_CLASS);
    }


    @OnClick(R.id.imv_back) void gotoBack(){
        startActivity(new Intent(this, SchoolMainActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}