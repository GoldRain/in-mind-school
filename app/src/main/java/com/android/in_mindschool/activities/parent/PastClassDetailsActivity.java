package com.android.in_mindschool.activities.parent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.BaseActivity;
import com.android.in_mindschool.adapter.ReviewListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PastClassDetailsActivity extends BaseActivity {

    ImageView imv_class, imv_location;
    TextView txv_location, txv_price, txv_rating, txv_type, txv_time,txv_completed,
            txv_about_us, txv_mon_fri_time, txv_sat_time, txv_sun_time, txv_your_rating, txv_content, txv_rating_done;
    EditText edt_your_review;
    RatingBar rating, your_rating, rating_done;

    @BindView(R.id.lyt_review_before) LinearLayout lyt_review_before;
    @BindView(R.id.lyt_review_done) LinearLayout lyt_review_done;
    @BindView(R.id.btn_review_class) Button btn_review_class;

    Float marks = 0.f;
    String reviews = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_class_details);

        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout(){

        imv_class = (ImageView)findViewById(R.id.imv_class);
        imv_location = (ImageView)findViewById(R.id.imv_location);
        txv_location = (TextView)findViewById(R.id.txv_location);
        txv_price = (TextView)findViewById(R.id.txv_price);
        rating = (RatingBar)findViewById(R.id.rating);
        txv_rating = (TextView)findViewById(R.id.txv_rating);
        txv_type = (TextView)findViewById(R.id.txv_type);
        txv_time = (TextView)findViewById(R.id.txv_time);
        txv_about_us = (TextView)findViewById(R.id.txv_about_us);
        txv_mon_fri_time = (TextView)findViewById(R.id.txv_mon_fri_time);
        txv_sat_time = (TextView)findViewById(R.id.txv_sat_time);
        txv_sun_time = (TextView)findViewById(R.id.txv_sun_time);
        txv_your_rating = (TextView)findViewById(R.id.txv_your_rating);
        your_rating = (RatingBar)findViewById(R.id.your_rating);
        rating_done = (RatingBar)findViewById(R.id.rating_done);

        //after rating
        rating_done.setIsIndicator(true);
        //marks
        txv_rating_done = (TextView)findViewById(R.id.txv_rating_done);
        txv_content = (TextView)findViewById(R.id.txv_content);

        edt_your_review = (EditText)findViewById(R.id.edt_your_review);


        your_rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                txv_your_rating.setText(String.valueOf(rating) + "/5");
                marks = rating;
            }
        });

    }

    @OnClick(R.id.btn_review_class) void reviewClass(){

        reviews = edt_your_review.getText().toString();

        if (reviews.isEmpty()){
            showToast("Write review");
            return;
        }

        lyt_review_before.setVisibility(View.GONE);
        lyt_review_done.setVisibility(View.VISIBLE);

        txv_content.setText(reviews);
        rating_done.setRating(marks);
        txv_rating_done.setText(marks + "/5");

        btn_review_class.setVisibility(View.GONE);

    }

    @OnClick(R.id.imv_back) void gotoBack(){
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}