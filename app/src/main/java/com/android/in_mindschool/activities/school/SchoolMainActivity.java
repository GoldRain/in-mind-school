package com.android.in_mindschool.activities.school;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.BaseActivity;
import com.android.in_mindschool.activities.LogInActivity;
import com.android.in_mindschool.activities.parent.ParentMainActivity;
import com.android.in_mindschool.common.Constants;
import com.android.in_mindschool.fragment.school.BookingManagementFragment;
import com.android.in_mindschool.fragment.school.FinancialHistoryManagmentFragment;
import com.android.in_mindschool.fragment.school.ManageClassFragment;
import com.android.in_mindschool.fragment.school.NotificationFragment;
import com.android.in_mindschool.fragment.school.ReportManagementFragment;
import com.android.in_mindschool.fragment.school.SettingsFragment;
import com.google.android.material.navigation.NavigationView;
import com.pixplicity.easyprefs.library.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;
import rebus.permissionutils.SmartCallback;

public class SchoolMainActivity extends BaseActivity implements View.OnClickListener{

    DrawerLayout drawerLayout;NavigationView navigationView;
    @BindView(R.id.imv_drawer) ImageView imv_drawer;
    @BindView(R.id.imv_noti) ImageView imv_noti;
    @BindView(R.id.txv_title) TextView txv_title;

    View headerView;
    LinearLayout lyt_manage_class, lyt_booking, lyt_financial, lyt_report, lyt_settings;
    ImageView imv_school;
    TextView txv_school_name, txv_logout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_main);
        ButterKnife.bind(this);
        checkPermission();
        loadLayout();
    }

    private void checkPermission() {

        PermissionManager.Builder()
                .permission(PermissionEnum.CAMERA, PermissionEnum.READ_EXTERNAL_STORAGE, PermissionEnum.WRITE_EXTERNAL_STORAGE
                        , PermissionEnum.ACCESS_FINE_LOCATION, PermissionEnum.ACCESS_COARSE_LOCATION)
                .askAgain(false)
                .callback(new SmartCallback() {
                    @Override
                    public void result(boolean allPermissionsGranted, boolean somePermissionsDeniedForever){
                    }
                })
                .ask(this);
    }

    private void loadLayout() {

        drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
        navigationView = (NavigationView)findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);

        imv_school = (ImageView) headerView.findViewById(R.id.imv_school);

        /*if (Commons.g_user.getPhoto_url().length() > 0)
            Glide.with(this).load(Commons.g_user.getPhoto_url()).placeholder(R.drawable.ic_profile).into(imv_photo);*/

        txv_school_name= (TextView)headerView.findViewById(R.id.txv_school_name);
        //txv_name.setText(Commons.g_user.getName());

        lyt_manage_class = (LinearLayout)headerView.findViewById(R.id.lyt_manage_class);
        lyt_manage_class.setOnClickListener(this);

        lyt_booking = (LinearLayout)headerView.findViewById(R.id.lyt_booking);
        lyt_booking.setOnClickListener(this);
        lyt_settings = (LinearLayout)headerView.findViewById(R.id.lyt_settings);
        lyt_settings.setOnClickListener(this);
        lyt_financial = (LinearLayout)headerView.findViewById(R.id.lyt_financial);
        lyt_financial.setOnClickListener(this);

        lyt_report = (LinearLayout)headerView.findViewById(R.id.lyt_report);
        lyt_report.setOnClickListener(this);

        txv_logout = (TextView)headerView.findViewById(R.id.txv_logout);
        txv_logout.setOnClickListener(this);

        imv_drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        imv_noti.setOnClickListener(this);

        gotoManageClassFragment();

        /*if (Constants.fragmentCount == 0)
            gotoHomeFragment();
        else if (Constants.fragmentCount == 2)
            gotoCartFragment();
        else if (Constants.fragmentCount == 3)
            gotoOrderHistoryFragment();*/
    }

    public void gotoManageClassFragment(){

        txv_title.setText(R.string.manage_class);
        notiShow(View.VISIBLE);

        ManageClassFragment fragment = new ManageClassFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoBookingManageFragment(){

        txv_title.setText(R.string.booking_management);
        notiShow(View.GONE);

        BookingManagementFragment fragment = new BookingManagementFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoFinancialManagementFragment(){

        txv_title.setText(R.string.financial_management);
        notiShow(View.GONE);

        FinancialHistoryManagmentFragment fragment = new FinancialHistoryManagmentFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoReportManagementFragment(){

        txv_title.setText(R.string.report_management);
        notiShow(View.GONE);

        ReportManagementFragment fragment = new ReportManagementFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoSettingsFragment(){

        txv_title.setText(R.string.settings);
        notiShow(View.GONE);

        SettingsFragment fragment = new SettingsFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoNotificationFragment(){

        txv_title.setText(R.string.noti);
        notiShow(View.GONE);

        NotificationFragment fragment = new NotificationFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    private void gotoLogout(){

        //registerToken();

        drawerLayout.closeDrawers();
        Prefs.putString(Constants.PREFKEY_USEREMAIL, "");
        Prefs.putString(Constants.PREFKEY_USERPWD, "");
        startActivity(new Intent(this, LogInActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.lyt_manage_class:
                gotoManageClassFragment();
                break;

            case R.id.lyt_booking:
                gotoBookingManageFragment();
                break;

            case R.id.lyt_financial:
                gotoFinancialManagementFragment();
                break;

            case R.id.lyt_report:
                gotoReportManagementFragment();
                break;

            case R.id.lyt_settings:
                gotoSettingsFragment();
                break;

            case R.id.imv_noti:
                gotoNotificationFragment();
                break;

            case R.id.txv_logout:
                gotoLogout();
                break;
        }
        drawerLayout.closeDrawers();
    }

    private void notiShow(int a){
        imv_noti.setVisibility(a);
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        } else onExit();
    }
}
