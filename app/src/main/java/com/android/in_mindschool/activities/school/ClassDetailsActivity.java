package com.android.in_mindschool.activities.school;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.BaseActivity;
import com.android.in_mindschool.activities.parent.ParentMainActivity;
import com.android.in_mindschool.adapter.EnrolledStudentsListAdapter;
import com.android.in_mindschool.adapter.ReviewListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ClassDetailsActivity extends BaseActivity {

    @BindView(R.id.lst_class_details)
    ListView lst_class_details;
    ReviewListAdapter adapter;
    EnrolledStudentsListAdapter adapter_enrolled;

    ImageView imv_class, imv_location;
    TextView txv_location, txv_price, txv_rating, txv_type, txv_time, txv_student,
            txv_attendance, txv_about_us, txv_mon_fri_time, txv_enrolled, txv_review, txv_all_reviews;
    RatingBar rating;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_details_school);
        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout(){

        View headerView =  ((LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                inflate(R.layout.header_class_details_school, null, false);

        imv_class = (ImageView)headerView.findViewById(R.id.imv_class);
        imv_location = (ImageView)headerView.findViewById(R.id.imv_location);
        txv_location = (TextView)headerView.findViewById(R.id.txv_location);
        txv_price = (TextView)headerView.findViewById(R.id.txv_price);
        rating = (RatingBar)headerView.findViewById(R.id.rating);
        txv_rating = (TextView)headerView.findViewById(R.id.txv_rating);
        txv_type = (TextView)headerView.findViewById(R.id.txv_type);
        txv_time = (TextView)headerView.findViewById(R.id.txv_time);
        txv_student = (TextView)headerView.findViewById(R.id.txv_student);
        txv_attendance = (TextView)headerView.findViewById(R.id.txv_attendance);
        txv_about_us = (TextView)headerView.findViewById(R.id.txv_about_us);
        txv_all_reviews = (TextView)headerView.findViewById(R.id.txv_all_reviews);
        //txv_mon_fri_time = (TextView)headerView.findViewById(R.id.txv_mon_fri_time);

        txv_enrolled = (TextView)headerView.findViewById(R.id.txv_enrolled);
        txv_review = (TextView)headerView.findViewById(R.id.txv_review);
        txv_enrolled.setSelected(true);

        txv_enrolled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectButton(true, false, getResources().getColor(R.color.white),
                        getResources().getColor(R.color.colorPrimary), R.string.enrolled_students);

                adapter_enrolled = new EnrolledStudentsListAdapter(v.getContext());
                lst_class_details.setAdapter(adapter_enrolled);

            }
        });

        txv_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectButton(false, true, getResources().getColor(R.color.colorPrimary),
                        getResources().getColor(R.color.white), R.string.all_reviews);

                adapter = new ReviewListAdapter(v.getContext());
                lst_class_details.setAdapter(adapter);
            }
        });

        lst_class_details.addHeaderView(headerView);
        adapter_enrolled = new EnrolledStudentsListAdapter(this);
        lst_class_details.setAdapter(adapter_enrolled);
        txv_all_reviews.setText(R.string.enrolled_students);
    }

    @OnClick(R.id.imv_delete) void deleteClass(){

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getResources().getString(R.string.you_want_delete_class));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _context.getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        showToast("deleted");
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, _context.getString(R.string.cancel),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        alertDialog.dismiss();
                    }
                });

        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.red));
    }

    @OnClick(R.id.imv_edit) void editClass(){

        Intent intent = new Intent(this, EditClassActivity.class);
        startActivity(intent);
        //finish();
    }

    private void selectButton(boolean a, boolean b, int c, int d, int e){

        txv_enrolled.setSelected(a);
        txv_review.setSelected(b);
        txv_enrolled.setTextColor(c);
        txv_review.setTextColor(d);
        txv_all_reviews.setText(e);
    }

    @OnClick(R.id.imv_back) void gotoBack(){
        startActivity(new Intent(ClassDetailsActivity.this, SchoolMainActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}