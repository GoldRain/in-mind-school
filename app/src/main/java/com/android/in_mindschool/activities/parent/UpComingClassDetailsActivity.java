package com.android.in_mindschool.activities.parent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.BaseActivity;
import com.android.in_mindschool.adapter.ReviewListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpComingClassDetailsActivity extends BaseActivity {

    //header
    ImageView imv_class, imv_location;
    TextView txv_location, txv_price, txv_rating, txv_type, txv_time,
            txv_about_us, txv_mon_fri_time, txv_sat_time, txv_sun_time;
    RatingBar rating;
    /***********-^-^-***********/

    @BindView(R.id.lst_upcoming_details) ListView lst_upcoming_details;
    @BindView(R.id.txv_start_time) TextView txv_start_time;
    ReviewListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_up_coming_class_details);
        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout(){

        View headerView =  ((LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                inflate(R.layout.header_upcoming_details, null, false);

        imv_class = (ImageView)headerView.findViewById(R.id.imv_class);
        imv_location = (ImageView)headerView.findViewById(R.id.imv_location);
        txv_location = (TextView)headerView.findViewById(R.id.txv_location);
        txv_price = (TextView)headerView.findViewById(R.id.txv_price);
        rating = (RatingBar)headerView.findViewById(R.id.rating);
        txv_rating = (TextView)headerView.findViewById(R.id.txv_rating);
        txv_type = (TextView)headerView.findViewById(R.id.txv_type);
        txv_time = (TextView)headerView.findViewById(R.id.txv_time);
        txv_about_us = (TextView)headerView.findViewById(R.id.txv_about_us);
        txv_mon_fri_time = (TextView)headerView.findViewById(R.id.txv_mon_fri_time);
        txv_sat_time = (TextView)headerView.findViewById(R.id.txv_sat_time);
        txv_sun_time = (TextView)headerView.findViewById(R.id.txv_sun_time);

        lst_upcoming_details.addHeaderView(headerView);
        adapter = new ReviewListAdapter(this);
        lst_upcoming_details.setAdapter(adapter);
    }

    @OnClick(R.id.imv_back) void gotoBack(){
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}