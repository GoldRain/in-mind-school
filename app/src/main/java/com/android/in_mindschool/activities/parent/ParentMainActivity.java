package com.android.in_mindschool.activities.parent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.BaseActivity;
import com.android.in_mindschool.activities.LogInActivity;
import com.android.in_mindschool.common.Commons;
import com.android.in_mindschool.common.Constants;
import com.android.in_mindschool.fragment.parent.HomeFragment;
import com.android.in_mindschool.fragment.parent.MyClassFragment;
import com.android.in_mindschool.fragment.parent.NotificationFragment;
import com.android.in_mindschool.fragment.parent.ProfileFragment;
import com.android.in_mindschool.fragment.parent.SettingsFragment;
import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.pixplicity.easyprefs.library.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;
import rebus.permissionutils.SmartCallback;

public class ParentMainActivity extends BaseActivity implements View.OnClickListener {

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    @BindView(R.id.imv_drawer) ImageView imv_drawer;
    @BindView(R.id.imv_noti) ImageView imv_noti;
    @BindView(R.id.imv_edit) ImageView imv_edit;

    View headerView;
    LinearLayout lyt_home, lyt_my_class, lyt_settings, lyt_profile;
    CircleImageView imv_photo;
    TextView txv_name, txv_logout;

    @BindView(R.id.txv_title) TextView txv_title;
    //ProfileFragment profileFragment = null;
    Uri _imageCaptureUri;
    String _photoPath;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_main);

        ButterKnife.bind(this);
        checkPermission();
        loadLayout();
    }

    private void checkPermission() {

        PermissionManager.Builder()
                .permission(PermissionEnum.CAMERA, PermissionEnum.READ_EXTERNAL_STORAGE, PermissionEnum.WRITE_EXTERNAL_STORAGE
                        , PermissionEnum.ACCESS_FINE_LOCATION, PermissionEnum.ACCESS_COARSE_LOCATION)
                .askAgain(false)
                .callback(new SmartCallback() {
                    @Override
                    public void result(boolean allPermissionsGranted, boolean somePermissionsDeniedForever){
                    }
                })
                .ask(this);
    }

    private void loadLayout() {

        drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
        navigationView = (NavigationView)findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);

        imv_photo = (CircleImageView)headerView.findViewById(R.id.imv_photo);

        /*if (Commons.g_user.getPhoto_url().length() > 0)
            Glide.with(this).load(Commons.g_user.getPhoto_url()).placeholder(R.drawable.ic_profile).into(imv_photo);*/

        txv_name = (TextView)headerView.findViewById(R.id.txv_name);
        //txv_name.setText(Commons.g_user.getName());

        lyt_home = (LinearLayout)headerView.findViewById(R.id.lyt_home);
        lyt_home.setOnClickListener(this);
        lyt_my_class = (LinearLayout)headerView.findViewById(R.id.lyt_my_class);
        lyt_my_class.setOnClickListener(this);
        lyt_settings = (LinearLayout)headerView.findViewById(R.id.lyt_settings);
        lyt_settings.setOnClickListener(this);
        lyt_profile = (LinearLayout)headerView.findViewById(R.id.lyt_profile);
        lyt_profile.setOnClickListener(this);
        txv_logout = (TextView)headerView.findViewById(R.id.txv_logout);
        txv_logout.setOnClickListener(this);

        imv_drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        imv_noti.setOnClickListener(this);
        imv_edit.setOnClickListener(this);

        gotoHomeFragment();

        /*if (Constants.fragmentCount == 0)
            gotoHomeFragment();
        else if (Constants.fragmentCount == 2)
            gotoCartFragment();
        else if (Constants.fragmentCount == 3)
            gotoOrderHistoryFragment();*/
    }

    public void gotoHomeFragment(){

        txv_title.setText(R.string.home);
        showNotiEditIcon(View.VISIBLE, View.GONE);

        HomeFragment fragment = new HomeFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoMyClassFragment(){

        txv_title.setText(R.string.my_class);
        showNotiEditIcon(View.GONE, View.GONE);

        MyClassFragment fragment = new MyClassFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoSettingsFragment(){

        txv_title.setText(R.string.settings);
        showNotiEditIcon(View.GONE, View.GONE);

        SettingsFragment fragment = new SettingsFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoProfileFragment(){

        txv_title.setText(R.string.profile);
        showNotiEditIcon(View.GONE, View.VISIBLE);

        ProfileFragment fragment = new ProfileFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoNotificationFragment(){

        txv_title.setText(R.string.noti);
        showNotiEditIcon(View.GONE, View.GONE);

        NotificationFragment fragment = new NotificationFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    private void gotoLogout(){

        //registerToken();

        drawerLayout.closeDrawers();
        Prefs.putString(Constants.PREFKEY_USEREMAIL, "");
        Prefs.putString(Constants.PREFKEY_USERPWD, "");
        startActivity(new Intent(this, LogInActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.lyt_home:
                gotoHomeFragment();
                break;

            case R.id.lyt_my_class:
                gotoMyClassFragment();
                break;

            case R.id.lyt_settings:
                gotoSettingsFragment();
                break;

            case R.id.lyt_profile:
                gotoProfileFragment();
                break;

            case R.id.imv_noti:
                gotoNotificationFragment();
                break;

            case R.id.txv_logout:
                gotoLogout();
                break;

            case R.id.imv_edit:
                break;

        }

        drawerLayout.closeDrawers();
    }

    private void showNotiEditIcon(int a, int b){
        imv_noti.setVisibility(a);
        imv_edit.setVisibility(b);
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        } else onExit();
    }
}
