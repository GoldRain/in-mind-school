package com.android.in_mindschool.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.in_mindschool.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends BaseActivity {

    @BindView(R.id.imv_back) ImageView imv_back;
    @BindView(R.id.edt_phone) EditText edt_phone;
    @BindView(R.id.edt_email) EditText edt_email;

    String phone_number = "", email = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout(){

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
                return false;
            }
        });
    }

    @OnClick(R.id.btn_send_link) void sendLink(){
        phone_number = edt_phone.getText().toString();
        email = edt_email.getText().toString();

        if (checkValid()){
            showToast("Send link");
        }

    }

    private boolean checkValid(){
        if (phone_number.isEmpty()){
            showToast(R.string.input_phone_number);
            return false;
        }
        else if(email.isEmpty()){
            showToast(R.string.input_your_email);

            return false;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            showToast(R.string.valid_email);
            return false;
        }

        return true;
    }

    @OnClick(R.id.imv_back) void gotoBack(){
        startActivity(new Intent(this, LogInActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}
