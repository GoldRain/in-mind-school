package com.android.in_mindschool.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.parent.AddLocationActivity;
import com.android.in_mindschool.activities.parent.ParentMainActivity;
import com.android.in_mindschool.activities.school.CreateProfileActivity;
import com.android.in_mindschool.activities.school.SchoolMainActivity;
import com.android.in_mindschool.common.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends BaseActivity {

    @BindView(R.id.edt_name) EditText edt_name;
    @BindView(R.id.edt_mobile_number) EditText edt_mobile_number;
    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_password) EditText edt_password;
    @BindView(R.id.txv_parent) TextView txv_parent;
    @BindView(R.id.txv_school) TextView txv_school;
    @BindView(R.id.checkbox) CheckBox checkbox;

    String name ="", mobile_number = "", email = "", password = "";

    String _type = Constants.PARENT;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout(){

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
                return false;
            }
        });

        txv_parent.setSelected(true);
    }

    @OnClick(R.id.txv_parent) void selectParent(){

        txv_school.setSelected(false);
        txv_parent.setSelected(true);
        txv_parent.setTextColor(getResources().getColor(R.color.white));
        txv_school.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    @OnClick(R.id.txv_school) void selectSchool(){

        txv_school.setSelected(true);
        txv_parent.setSelected(false);
        txv_school.setTextColor(getResources().getColor(R.color.white));
        txv_parent.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    @OnClick(R.id.btn_signUp) void gotoSignUp(){

        name = edt_name.getText().toString();
        mobile_number = edt_mobile_number.getText().toString();
        email = edt_email.getText().toString();
        password = edt_password.getText().toString();

        getType();

        //if (checkValid()){

            if (txv_parent.isSelected()){

                //processLogin(Constants.PARENT);
                startActivity(new Intent(SignUpActivity.this, AddLocationActivity.class));
                finish();
            }
            else if (txv_school.isSelected()){
                //showToast("Coming soon");
                //processLogin(Constants.SCHOOL);
                startActivity(new Intent(SignUpActivity.this, CreateProfileActivity.class));
                finish();
            }
        //}
    }

    @OnClick(R.id.txv_login) void gotoLogin(){

        startActivity(new Intent(SignUpActivity.this, LogInActivity.class));
        finish();
    }


    private void getType(){

        if (txv_parent.isSelected())
            _type = Constants.PARENT;
        else if (txv_school.isSelected())
            _type = Constants.SCHOOL;

    }

    private boolean checkValid(){

        if (name.isEmpty()){
            showToast(R.string.input_name);
            return false;
        }
        else if (mobile_number.isEmpty()){
            showToast(R.string.input_phone_number);
            return false;
        }
        else if (email.isEmpty()){
            showToast(R.string.input_your_email);
            return false;
        }else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            showToast(R.string.valid_email);
            return false;
        }else if (password.isEmpty()){
            showToast(R.string.input_password);
            return false;
        }
        else if (!checkbox.isChecked()){
            showToast(R.string.accept_terms);
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        gotoLogin();
    }
}
