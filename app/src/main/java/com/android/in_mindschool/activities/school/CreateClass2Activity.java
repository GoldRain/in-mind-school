package com.android.in_mindschool.activities.school;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.in_mindschool.R;
import com.android.in_mindschool.activities.BaseActivity;
import com.android.in_mindschool.rangeDatePicker.CalendarPickerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateClass2Activity extends BaseActivity {

    CalendarPickerView calendar;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_class2);
        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout() {

        initCalendarView();
    }

    private void initCalendarView(){

        calendar = findViewById(R.id.calendar_view);
        /*Last year*/
        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);
        /*Next year*/
        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, 0);

        ArrayList<Integer> list = new ArrayList<>();

        ArrayList<Date> arrayList = new ArrayList<>();
        try {
            SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
            String strdate = "22-4-2019";
            Date newdate = dateformat.parse(strdate);
            arrayList.add(newdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.scrollToDate(new Date());
        calendar.init(lastYear.getTime(), nextYear.getTime(), new SimpleDateFormat("MMM, yyyy", Locale.getDefault())/*, TimeZone.getTimeZone(Commons.thisSeller.getTimezone())*/) //
                .inMode(CalendarPickerView.SelectionMode.RANGE) //
                .withDeactivateDates(list);
        //.withSubTitles(getSubTitles())
        //.withHighlightedDates(list);

        calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                calendar.getSelectedDates();

                if (calendar.getSelectedDates().size() == 1){
                    String startDate = calendar.getSelectedDates().get(0).toString().substring(0, 11);
                    //txv_dates.setText(startDate);
                }
                else {
                    int idx = calendar.getSelectedDates().size();
                    String dateArray[] = calendar.getSelectedDates().get(0).toString().split(" ");

                    String startDate = calendar.getSelectedDates().get(0).toString().substring(0, 11);
                    String endDate = calendar.getSelectedDates().get(idx - 1).toString().substring(0, 11);
                    //txv_dates.setText(startDate +" - " + endDate);

                }
            }

            @Override
            public void onDateUnselected(Date date) {

            }
        });

    }

    @OnClick(R.id.txv_done) void createClass(){

        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.imv_back) void gotoBack(){

        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}