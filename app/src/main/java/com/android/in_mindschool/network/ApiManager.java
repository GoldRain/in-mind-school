package com.android.in_mindschool.network;

import android.util.Log;

import com.android.in_mindschool.utils.JsonUtils;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.internal.Constants;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.android.in_mindschool.network.ApiManager.PARAMS.RESULT_CODE;


public class ApiManager {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final MediaType JPEG = MediaType.parse("image/jpeg");
    public static final MediaType PNG = MediaType.parse("image/png");
    public static final MediaType VIDEO = MediaType.parse("video/mp4");

    private static final String HOST = "http://meraindia.com/";
    public static final String API = HOST + "api/";
    private static final String USER = "user/";
    private static final String POST = "post/";
    private static final String PRODUCT = "product/";


    private static final String REGISTER = "register";
    private static final String LOGIN = "login";
    private static final String SOCIAL_LOGIN = "social_login";
    private static final String FORGOT = "forgot";
    private static final String RESET_PASSWORD = "reset_password";
    private static final String REGISTER_TOKEN = "register_token";
    private static final String GET_ALL_POST = "get_all_post";
    private static final String ADD_POST = "add_post";
    private static final String ADD_PAGE = "add_page";
    private static final String ADD_GROUP = "add_group";
    private static final String GET_GROUP = "get_group";
    private static final String ADD_EVENT = "add_event";
    private static final String GET_WAREHOUSE = "get_warehouse";
    private static final String GET_COURIERSERVICE = "get_courierservice";
    private static final String ADD_PRODUCT = "add_product";
    private static final String GET_PRODUCT = "get_product";
    private static final String PRODUCT_DETAIL = "product_detail";
    private static final String ADD_WAREHOUSE = "add_warehouse";
    private static final String ADD_COURIERSERVICE = "add_courierservice";
    private static final String ADD_TO_CART = "add_to_cart";
    private static final String GET_CART_PRODUCT = "get_cart_product";
    private static final String INCREASE_CART_COUNT = "increase_cart_count";
    private static final String DECREASE_CART_COUNT = "decrease_cart_count";
    private static final String DELETE_CART = "delete_cart";
    private static final String EDIT_USER = "edit_user";
    private static final String UPLOAD_PHOTO = "upload_photo";
    private static final String UPLOAD_DRIVER_LICENSE = "upload_driver_license";
    public static final String PAY_PRODUCT = "pay_product";
    private static final String GET_ORDER = "get_order";
    private static final String GET_TRANSACTION = "get_transaction";
    private static final String GET_ALL_ORDERS = "get_all_orders";
    private static final String PLACE_BID = "place_bid";
    private static final String EDIT_BID = "edit_bid";
    private static final String GET_BID_COURIERS = "get_bid_couriers";
    private static final String GET_ALL_BID = "get_all_bid";
    private static final String GET_TRACKING_STATUS = "get_tracking_status";
    private static final String GET_ADDRESS = "get_address";
    private static final String ADD_ADDRESS = "add_address";
    private static final String SET_TRACKING_STATUS = "set_tracking_status";
    private static final String STRIPE_PAYMENT = "stripe_payment";
    public static final String ADD_ORDER = "add_order";
    private static final String AWARD_BID = "award_bid";
    private static final String GET_NOTIFICATION = "get_notification";
    private static final String GET_AWARDED_BID = "get_awarded_bid";


    private static final int RESULT_OK = 200;

/*
    public static void register(String first_name, String last_name, String email, String password, String city, File photo, final ICallback callback) {

        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart(PARAMS.FIRST_NAME, first_name)
                .addFormDataPart(PARAMS.LAST_NAME, last_name)
                .addFormDataPart(PARAMS.EMAIL, email)
                .addFormDataPart(PARAMS.PASSWORD, password)
                .addFormDataPart(PARAMS.CITY, city)
                .addFormDataPart(PARAMS.PHOTO, photo.getName(), RequestBody.create(photo, MediaType.parse("image/*")))
                .build();

        WebManager.call(USER + REGISTER, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK){

                        Commons.g_user.setPhoto_url(json.get(PARAMS.PHOTO_URL).getAsString());
                        Commons.g_user.setId(json.get(PARAMS.ID).getAsInt());

                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    }
                    else callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);

                }
                catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
            }
        });

    }


    public static void login(String email, String password*//*, String type*//*, final ICallback callback){

        RequestBody body = new FormBody.Builder()
                .add(PARAMS.EMAIL, email)
                .add(PARAMS.PASSWORD, password)
                *//*.add(PARAMS.TYPE, type)*//*
                .build();

        WebManager.call(USER + LOGIN, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();

                    Log.d("login==>", json.toString());

                    int resultCode = JsonUtils.getInt(json.get(RESULT_CODE));

                    if (resultCode == RESULT_OK){

                        UserModel user = new UserModel();

                        JsonObject jsonUser = JsonUtils.getJsonObject(json.get(PARAMS.USER_DATA));

                        user.setId(jsonUser.get(PARAMS.ID).getAsInt());
                        user.setFirst_name(jsonUser.get(PARAMS.FIRST_NAME).getAsString());
                        user.setLast_name(jsonUser.get(PARAMS.LAST_NAME).getAsString());
                        user.setEmail(jsonUser.get(PARAMS.EMAIL).getAsString());
                        user.setCity(jsonUser.get(PARAMS.CITY).getAsString());
                        user.setPhoto_url(jsonUser.get(PARAMS.PHOTO_URL).getAsString());
                        user.setLogin_type(jsonUser.get(PARAMS.LOGIN_TYPE).getAsString());

                        Commons.g_user = user;

                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    }
                    else {
                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }
                } catch (Exception e) {
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
            }
        });
    }

    public static void registerToken(final String token, final ICallback callback){

        String url = API + USER + REGISTER_TOKEN;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject json = new JSONObject(response);
                    int resultCode = json.getInt(RESULT_CODE);

                    if (resultCode != 200) {

                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }
                    else callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);


                } catch (JSONException e) {
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(PARAMS.USER_ID, String.valueOf(1*//*Commons.g_user.getId())*//*));
                    params.put(PARAMS.TOKEN, token);

                } catch (Exception e) {

                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MeralndiaApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public static void socialLogin(String first_name, String last_name, String email, String social_id, String photo_url, String login_type, final ICallback callback){

        RequestBody body = new FormBody.Builder()
                .add(PARAMS.FIRST_NAME, first_name)
                .add(PARAMS.LAST_NAME, last_name)
                .add(PARAMS.EMAIL, email)
                .add(PARAMS.SOCIAL_ID, social_id)
                .add(PARAMS.LOGIN_TYPE, login_type)
                .add(PARAMS.PHOTO_URL, photo_url)
                .build();

        WebManager.call(USER + SOCIAL_LOGIN, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK) {

                        JsonObject jsonUser = json.get(PARAMS.USER_DATA).getAsJsonObject();

                        UserModel user = new UserModel();

                        user.setId(jsonUser.get(PARAMS.ID).getAsInt());
                        user.setFirst_name(jsonUser.get(PARAMS.FIRST_NAME).getAsString());
                        user.setLast_name(jsonUser.get(PARAMS.LAST_NAME).getAsString());
                        user.setEmail(jsonUser.get(PARAMS.EMAIL).getAsString());
                        user.setCity(jsonUser.get(PARAMS.CITY).getAsString());
                        user.setPhoto_url(jsonUser.get(PARAMS.PHOTO_URL).getAsString());
                        user.setLogin_type(jsonUser.get(PARAMS.LOGIN_TYPE).getAsString());

                        Commons.g_user = user;

                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);

                    } else if (resultCode == 203){

                        int id = json.get(PARAMS.ID).getAsInt();

                        UserModel user = new UserModel();

                        user.setId(id);
                        user.setFirst_name(first_name);
                        user.setLast_name(last_name);
                        user.setEmail(email);
                        user.setPhoto_url(photo_url);
                        user.setLogin_type(login_type);

                        Commons.g_user = user;

                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    }
                    else callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);

                }
                catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
            }
        });
    }


    public static void forgotPassword(String email, final ICallback callback){

        RequestBody body = new FormBody.Builder()
                .add(PARAMS.EMAIL, email)
                .build();

        WebManager.call(USER + FORGOT, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK){

                        String pin_code = json.get(PARAMS.PINCODE).getAsString();

                        callback.onCompletion(ICallback.RESULT.SUCCESS, pin_code);
                    }
                    else callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                }
                catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }

            }
        });
    }

    public static void resetPassword(String email, String password, final ICallback callback){

        RequestBody body = new FormBody.Builder()
                .add(PARAMS.EMAIL, email)
                .add(PARAMS.PASSWORD, password)
                .build();

        WebManager.call(USER + RESET_PASSWORD, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK){

                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    }
                    else callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                }
                catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }

            }
        });
    }

    public static void getAllPost(final ICallback callback){

        RequestBody body = new FormBody.Builder()
                .build();

        WebManager.call(POST + GET_ALL_POST, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK){

                        JsonArray json_array = json.get(PARAMS.POST_LIST).getAsJsonArray();
                        ArrayList<PostModel> arrayPost = new ArrayList<>();

                        for (int i = 0; i < json_array.size(); i++){

                            JsonObject jsonPost = (JsonObject)json_array.get(i);

                            PostModel post_list = new PostModel();

                            post_list.setId(jsonPost.get(PARAMS.ID).getAsInt());
                            post_list.setUser_id(jsonPost.get(PARAMS.USER_ID).getAsInt());
                            post_list.setCity(jsonPost.get(PARAMS.CITY).getAsString());
                            post_list.setType(jsonPost.get(PARAMS.TYPE).getAsString());
                            post_list.setDescription(jsonPost.get(PARAMS.DESCRIPTION).getAsString());
                            post_list.setTag(jsonPost.get(PARAMS.TAG).getAsString());
                            post_list.setComment_count(jsonPost.get(PARAMS.COMMENT_COUNT).getAsInt());
                            post_list.setLike_count(jsonPost.get(PARAMS.LIKE_COUNT).getAsInt());
                            post_list.setCreated_at(jsonPost.get(PARAMS.CREATE_AT).getAsString());
                            post_list.setFirst_name(jsonPost.get(PARAMS.FIRST_NAME).getAsString());
                            post_list.setLast_name(jsonPost.get(PARAMS.LAST_NAME).getAsString());
                            post_list.setUser_photo_url(jsonPost.get(PARAMS.USER_PHOTO_URL).getAsString());
                            //post_list.setMedia_id(jsonPost.get(PARAMS.MEDIA_ID).getAsInt());
                            //post_list.setFile_url(jsonPost.get(PARAMS.FILE_URL).getAsString());

                            ArrayList<String> file_urls = new ArrayList<>();
                            JsonArray jsonUrls = jsonPost.get(PARAMS.FILE_URLS).getAsJsonArray();
                            for (int j = 0; j < jsonUrls.size(); j++){
                                file_urls.add(jsonUrls.get(j).getAsString());
                            }
                            post_list.setFile_urls(file_urls);

                            arrayPost.add(post_list);
                        }

                        callback.onCompletion(ICallback.RESULT.SUCCESS, arrayPost);
                    }
                    else callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                }
                catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }

            }
        });
    }

    public static void editUser(int id, String name, String email, String phone, String address,
                                String state, String city, String zipcode, String category, String payment_type,
                                String country, int warehouse_id, int courier_id, final ICallback callback){

        RequestBody body = new FormBody.Builder()
                .add(PARAMS.USER_ID, String.valueOf(id))
                .add(PARAMS.NAME, name)
                .add(PARAMS.EMAIL, email)
                .add(PARAMS.PHONE, phone)
                .add(PARAMS.ADDRESS, address)
                .add(PARAMS.STATE, state)
                .add(PARAMS.CITY, city)
                .add(PARAMS.ZIPCODE, zipcode)
                .add(PARAMS.CATEGORY, category)
                .add(PARAMS.PAYMENT_TYPE, payment_type)
                .add(PARAMS.COUNTRY, country)
                .add(PARAMS.WAREHOUSE_ID, String.valueOf(warehouse_id))
                .add(PARAMS.COURIER_ID, String.valueOf(courier_id))
                .build();

        WebManager.call(USER + EDIT_USER, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {
                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK){

                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    }
                }
                catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
            }
        });
    }

    public static void uploadPhoto(int user_id, File photo, final ICallback callback){

        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart(PARAMS.USER_ID, String.valueOf(user_id))
                .addFormDataPart(PARAMS.PHOTO, photo.getName(), RequestBody.create(photo, MediaType.parse("image/*")))
                .build();

        WebManager.call(USER + UPLOAD_PHOTO, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK){

                        String photoURL = json.get(PARAMS.PHOTO_URL).getAsString();

                        callback.onCompletion(ICallback.RESULT.SUCCESS, photoURL);
                    }
                }
                catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
            }
        });
    }


    public static void addPost(int user_id, String description, List<MediaModel> post_paths,String state, String city, String tag, String type, String invites, final ICallback callback){

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        builder.addFormDataPart(PARAMS.USER_ID, String.valueOf(user_id));
        builder.addFormDataPart(PARAMS.DESCRIPTION, description);
        *//*builder.addFormDataPart(PARAMS.STATE, state);*//*
        builder.addFormDataPart(PARAMS.CITY, city);
        builder.addFormDataPart(PARAMS.TAG, tag);
        builder.addFormDataPart(PARAMS.TYPE, type);
        builder.addFormDataPart(PARAMS.INVITES, invites);

        //File part


        if (post_paths.size() > 0) {

            for (MediaModel path : post_paths) {

                File file = new File(path.get_path());
                final MediaType MEDIA_TYPE = MediaType.parse("image/*");
                builder.addFormDataPart("post[]", file.getName(), RequestBody.create(file, MEDIA_TYPE));
            }
        }

        RequestBody body = builder.build();

        WebManager.call(POST + ADD_POST, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    Log.d("result==>", response.toString());

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    Log.d("result==>", json.toString());

                    if (resultCode == RESULT_OK){

                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    }

                }catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
            }
        });

    }


    public static void addPage(String name, int user_id,String state, String city, String tag, File profile, File cover,String inviteFriends, final ICallback callback){

         MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
         builder.addFormDataPart(PARAMS.NAME, name);

         builder.addFormDataPart(PARAMS.USER_ID, String.valueOf(user_id));
         *//*builder.addFormDataPart(PARAMS.STATE, state)*//*
        builder.addFormDataPart(PARAMS.CITY, city);
        builder.addFormDataPart(PARAMS.TAG, tag);

        if (profile != null)
            builder.addFormDataPart(PARAMS.PROFILE, profile.getName(), RequestBody.create(profile, MediaType.parse("image/*")));

        if (cover != null)
            builder.addFormDataPart(PARAMS.COVER, cover.getName(), RequestBody.create(cover, MediaType.parse("image/*")));

        builder.addFormDataPart(PARAMS.INVITES, inviteFriends);

        RequestBody body = builder.build();


        WebManager.call(POST + ADD_PAGE, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK){

                        //String photoURL = json.get(PARAMS.PHOTO_URL).getAsString();

                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    }
                }
                catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
            }
        });
    }

    public static void addGroup(String name, int user_id,String state, String city, String tag, File profile, File cover, String inviteFriends, final ICallback callback){

         MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart(PARAMS.NAME, name);
        builder.addFormDataPart(PARAMS.USER_ID, String.valueOf(user_id));
        builder.addFormDataPart(PARAMS.STATE, state);
        builder.addFormDataPart(PARAMS.CITY, city);
        builder.addFormDataPart(PARAMS.TAG, tag);

        if (profile != null)
            builder.addFormDataPart(PARAMS.PROFILE, profile.getName(), RequestBody.create(profile, MediaType.parse("image/*")));
        if (cover != null)
            builder.addFormDataPart(PARAMS.COVER, cover.getName(), RequestBody.create(cover, MediaType.parse("image/*")));

        builder.addFormDataPart(PARAMS.INVITES, inviteFriends);

        RequestBody body = builder.build();

        WebManager.call(POST + ADD_GROUP, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK){

                        //String photoURL = json.get(PARAMS.PHOTO_URL).getAsString();

                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    }
                }
                catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
            }
        });
    }

    public static void getGroup(int user_id, final ICallback callback){

        RequestBody body = new FormBody.Builder()
                .add(PARAMS.USER_ID, String.valueOf(user_id))
                .build();

        WebManager.call(POST + GET_GROUP, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {
                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK){

                        JsonArray jsonArray = json.get(PARAMS.GROUP_LIST).getAsJsonArray();
                        ArrayList<GroupModel> groups = new ArrayList<>();

                        for (int i = 0; i < jsonArray.size(); i++){

                            JsonObject jsonGroup = (JsonObject)jsonArray.get(i);

                            GroupModel group = new GroupModel();

                            group.setId(jsonGroup.get(PARAMS.ID).getAsInt());
                            group.setName(jsonGroup.get(PARAMS.NAME).getAsString());
                            group.setUser_id(jsonGroup.get(PARAMS.USER_ID).getAsInt());
                            group.setCity(jsonGroup.get(PARAMS.CITY).getAsString());
                            group.setTag(jsonGroup.get(PARAMS.TAG).getAsString());
                            group.setInvites(jsonGroup.get(PARAMS.INVITES).getAsString());
                            group.setPhotoUrl(jsonGroup.get(PARAMS.PROFILE_URL).getAsString());
                            group.setPhotoUrl(jsonGroup.get(PARAMS.COVER_URL).getAsString());
                            group.setCreated_at(jsonGroup.get(PARAMS.CREATE_AT).getAsString());
                            group.setUpdated_at(jsonGroup.get(PARAMS.UPDATED_AT).getAsString());

                            groups.add(group);
                        }

                        callback.onCompletion(ICallback.RESULT.SUCCESS, groups);
                    }
                    else {
                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }
                }
                catch (Exception e) {
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
            }
        });
    }

    public static void addEvent(String name, int user_id, String state, String city, String tag, File profile, File cover, String startDate, String endDate, String inviteFriends, final ICallback callback){

         MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart(PARAMS.NAME, name);
        builder.addFormDataPart(PARAMS.USER_ID, String.valueOf(user_id));
                *//*builder.addFormDataPart(PARAMS.STATE, state)*//*
        builder.addFormDataPart(PARAMS.CITY, city);
        builder.addFormDataPart(PARAMS.TAG, tag);
        builder.addFormDataPart(PARAMS.START_DATE, startDate);
        builder.addFormDataPart(PARAMS.END_DATE, endDate);

        if (profile != null)
            builder.addFormDataPart(PARAMS.PROFILE, profile.getName(), RequestBody.create(profile, MediaType.parse("image/*")));
        if (cover != null)
            builder.addFormDataPart(PARAMS.COVER, cover.getName(), RequestBody.create(cover, MediaType.parse("image/*")));

        builder.addFormDataPart(PARAMS.INVITES, inviteFriends);

        RequestBody body = builder.build();

        WebManager.call(POST + ADD_EVENT, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK){

                        //String photoURL = json.get(PARAMS.PHOTO_URL).getAsString();

                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    }
                }
                catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
            }
        });
    }

    public static void addAddress(int user_id, String name, String address, String city, String state, String zipcode, final ICallback callback){

        RequestBody body = new FormBody.Builder()
                .add(PARAMS.USER_ID, String.valueOf(user_id))
                .add(PARAMS.NAME, name)
                .add(PARAMS.ADDRESS, address)
                .add(PARAMS.CITY, city)
                .add(PARAMS.STATE, state)
                .add(PARAMS.ZIPCODE, zipcode)
                .build();

        WebManager.call(PRODUCT + ADD_ADDRESS, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK){

                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);

                    }
                }
                catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
            }
        });
    }


    public static void getNotification(int user_id, final ICallback callback){

        RequestBody body = new FormBody.Builder()
                .add(PARAMS.USER_ID, String.valueOf(user_id))
                .build();

        WebManager.call(USER + GET_NOTIFICATION, WebManager.METHOD.POST, body, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onCompletion(ICallback.RESULT.FAILURE, null);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    JsonObject json = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    int resultCode = json.get(RESULT_CODE).getAsInt();

                    if (resultCode == RESULT_OK){

                        JsonArray arrayJson = json.get(PARAMS.NOTIFICATION_LIST).getAsJsonArray();

                        callback.onCompletion(ICallback.RESULT.SUCCESS, arrayJson);
                    }
                }
                catch (Exception e){
                    callback.onCompletion(ICallback.RESULT.FAILURE, null);
                }
            }
        });
    }*/



    public static class PARAMS {

        private static final String TOKEN = "token";
        public static final String PHONE = "phone";
        public static final String PASSWORD = "password";
        public static final String USER_DATA = "user_data";
        public static final String ID = "id";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String USER_ID = "user_id";
        public static final String PINCODE = "pincode";
        public static final String NAME = "name";
        public static final String EMAIL = "email";
        public static final String PHOTO_URL = "photo_url";
        public static final String SOCIAL_ID = "social_id";
        public static final String PAYMENT_TYPE = "payment_type";
        public static final String LOGIN_TYPE = "login_type";
        public static final String POST_LIST = "post_list";
        public static final String TAG = "tag";
        public static final String START_DATE = "start_date";
        public static final String END_DATE = "end_date";
        public static final String COMMENT_COUNT = "comment_count";
        public static final String LIKE_COUNT = "like_count";
        public static final String USER_PHOTO_URL = "user_photo_url";
        public static final String MEDIA_ID = "media_id";
        public static final String FILE_URL = "file_url";
        public static final String FILE_URLS = "file_urls";
        public static final String STATE = "state";
        public static final String CITY = "city";
        public static final String ZIPCODE = "zipcode";
        public static final String COUNTRY = "country";
        public static final String WAREHOUSE_ID = "warehouse_id";
        public static final String COURIER_ID = "courier_id";
        public static final String PHOTO = "photo";
        public static final String PROFILE = "profile";
        public static final String COVER = "cover";
        public static final String PROFILE_URL = "profile_url";
        public static final String COVER_URL = "cover_url";
        public static final String INVITES = "invites";
        public static final String GROUP_LIST = "group_list";
        public static final String DRIVER_LICENSE = "driver_license";
        public static final String WAREHOUSE_LIST = "warehouse_list";
        public static final String COURIERSERVICE_LIST = "courierservice_list";
        public static final String QUANTITY = "quantity";
        public static final String WAREHOUSE_LOCATION = "warehouse_location";
        public static final String BRAND = "brand";
        public static final String PRICE = "price";
        public static final String USER_NAME = "user_name";
        public static final String TIME_DURATION = "time_duration";
        public static final String PAGE = "page";
        public static final String PRODUCT_LIST = "product_list";
        public static final String PRODUCT_CODE = "product_code";
        public static final String USER_CODE = "user_code";
        public static final String PRODUCT_DETAIL = "product_detail";
        public static final String PRODUCT_ID = "product_id";
        public static final String PRODUCT_NAME = "product_name";
        public static final String COUNT = "count";
        public static final String USERID = "userid";
        public static final String LOCATION = "location";
        public static final String CREATE_AT = "created_at";
        public static final String COMPANY_NAME = "company_name";
        public static final String CART_LIST = "cart_list";
        public static final String PRODUCT = "product";
        public static final String OWNER = "owner";
        public static final String IMAGE_URLS = "image_urls";
        public static final String ORDER_LIST = "order_list";
        public static final String TRANSACTION_LIST = "transaction_list";
        public static final String ORDER_ID = "order_id";
        public static final String COST = "cost";
        public static final String BID_ID = "bid_id";
        public static final String COURIER_LIST = "courier_list";
        public static final String BID_LIST = "bid_list";

        public static final String CODE = "code";
        public static final String RESULT_CODE = "result_code";
        public static final String CATEGORY = "category";
        public static final String TIME = "time";
        public static final String TYPE = "type";
        public static final String DESCRIPTION = "description";
        public static final String ADDRESS = "address";
        public static final String CART_ID = "cart_id";
        public static final String COURIER_NAME = "courier_name";
        public static final String BUYER_ID = "buyer_id";
        public static final String BUYER_NAME = "buyer_name";
        public static final String STATUS = "status";
        public static final String FROM = "from";
        public static final String TO = "to";
        public static final String AMOUNT = "amount";
        public static final String PAY_TYPE = "pay_type";
        public static final String TRANSACTION_ID = "transaction_id";
        public static final String ADDRESS_LIST = "address_list";
        public static final String IS_AWARDED = "is_awarded"; //awarded - is_awarded
        public static final String SELLER_NAME = "seller_name";
        public static final String BID_COUNT = "bid_count";
        public static final String IS_BID = "is_bid";
        public static final String SOLD_COUNT = "sold_count";
        public static final String PICKED_UP = "picked_up";
        public static final String ARRIVING_TIME = "arriving_time";
        public static final String DELIVERED_NEAREST = "delivered_nearest";
        public static final String DELIVERED_SUCCESS = "delivered_success";
        public static final String TRACK_STATUS = "track_status";
        public static final String STRIPE_TOKEN = "stripe_token";
        public static final String MESSAGE = "message";
        public static final String IS_PUBLIC = "is_public";
        public static final String COMPLETED_AT = "completed_at";
        public static final String NOTIFICATION_LIST = "notification_list";
        public static final String CONTENT = "content";
        public static final String TITLE = "title";
        public static final String BID = "bid";
        public static final String UPDATED_AT = "updated_at";
        public static final String WEIGHT = "weight";
        public static final String SIZE = "size";

    }
}


