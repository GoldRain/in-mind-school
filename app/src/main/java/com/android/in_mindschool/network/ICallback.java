package com.android.in_mindschool.network;

/**
 * Created by JIS on 2/1/2018.
 */

public interface ICallback {

    public enum RESULT {SUCCESS, FAILURE};
    public void onCompletion(RESULT result, Object resultParam);

}
